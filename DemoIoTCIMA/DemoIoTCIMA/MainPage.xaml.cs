﻿using IoTCloud2019.COMMON.Modelos;
using OpenNETCF.MQTT;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DemoIoTCIMA
{
    public partial class MainPage : ContentPage
    {
        CimaViewModel model;
        public MainPage()
        {
            InitializeComponent();
            model = new CimaViewModel();
            BindingContext = model;
        }

        private void SwtLed_Toggled(object sender, ToggledEventArgs e)
        {
            model.CambiarLed(e.Value);
        }

        private void SwtBuzzer_Toggled(object sender, ToggledEventArgs e)
        {
            model.CambiarBuzzer(e.Value);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            model.ActualizarLecturas();
            var lecturas = model.lecturasTemperatura;
            Graficar(lecturas, plotTemperatura, "Temperatura");
            Graficar(model.lecturasHumedad, plotHumedad, "Humedad");
            Graficar(model.lecturasLuminosidad, plotLuminosidad, "Luminosidad");
        }

        private void Graficar(List<ModelLectura> lecturas, PlotView plot, string titulo)
        {
            PlotModel grafico = new PlotModel();
            LinearAxis axisX = new LinearAxis();
            axisX.Position = AxisPosition.Bottom;
            LinearAxis axisY = new LinearAxis();
            axisY.Position = AxisPosition.Left;
            grafico.Axes.Add(axisX);
            grafico.Axes.Add(axisY);
            grafico.Title = titulo;
            LineSeries linea = new LineSeries();
            int i = 0;
            foreach (var punto in lecturas)
            {
                linea.Points.Add(new DataPoint(i, punto.Lectura.Valor));
                i++;
            }
            linea.Color = OxyColors.DarkBlue;
            
            grafico.Series.Add(linea);
            plot.HorizontalOptions = LayoutOptions.Fill;
            plot.Model = grafico;
        }
    }
}
