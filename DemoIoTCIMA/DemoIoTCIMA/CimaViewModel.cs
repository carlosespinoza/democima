﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using OpenNETCF.MQTT;
using Xamarin.Forms;
using IoTCloud2019.COMMON.Modelos;
using IoTCloud2019.COMMON.Interfaces;
using IoTCloud2019.BIZ;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace DemoIoTCIMA
{
    public class CimaViewModel : ViewModelBase
    {
        MQTTClient mqtt;
        int pulsos;
        ILecturaManager lecturaManager;
        public string UltimoComando { get; set; }
        public string Pulsos { get; set; }
        public ICommand ConectarCommand { get; set; }
        public PlotModel GraficoTemperatura { get; set; }
        public PlotModel GraficoHumedad { get; set; }
        public PlotModel GraficoLuminosidad { get; set; }
        public List<ModelLectura> lecturasTemperatura { get; set; }
        public List<ModelLectura> lecturasHumedad { get; set; }
        public List<ModelLectura> lecturasLuminosidad { get; set; }
        public CimaViewModel()
        {
            mqtt = new MQTTClient("broker.hivemq.com", 1883);
            mqtt.Connected += Mqtt_Connected;
            mqtt.MessageReceived += Mqtt_MessageReceived;
            mqtt.Disconnected += Mqtt_Disconnected;
            UltimoComando = "Sin Conexión :(";
            Notify("UltimoComando");
            ConectarCommand = new Command(Conectar);
            pulsos = 0;
            ActualizaPulsos();
            lecturaManager = FabricManager.LecturaManager();
        }

        private void ActualizaPulsos()
        {
            Pulsos = $"{pulsos} Pulsos";
            Notify("Pulsos");
        }

        private void Mqtt_Disconnected(object sender, EventArgs e)
        {
            UltimoComando = "Desconectado :(";
            Notify("UltimoComando");
        }

        public void CambiarLed(bool status)
        {
            if (status)
            {
                mqtt.Publish("IoTCloud2019/IoTBoard", "L1", QoS.FireAndForget, false);
            }
            else
            {
                mqtt.Publish("IoTCloud2019/IoTBoard", "L0", QoS.FireAndForget, false);
            }
        }

        public void CambiarBuzzer(bool status)
        {
            if (status)
            {
                mqtt.Publish("IoTCloud2019/IoTBoard", "B1", QoS.FireAndForget, false);
            }
            else
            {
                mqtt.Publish("IoTCloud2019/IoTBoard", "B0", QoS.FireAndForget, false);
            }
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string mensaje = System.Text.Encoding.UTF8.GetString(payload);
            UltimoComando = mensaje;
            Notify("UltimoComando");
            if (mensaje == "Pulso")
            {
                pulsos++;
                ActualizaPulsos();
            }
        }

        private void Mqtt_Connected(object sender, EventArgs e)
        {
            UltimoComando = "Conectado :)";
            Notify("UltimoComando");
            mqtt.Subscriptions.Add(new Subscription("IoTCloud2019/IoTBoard"));
        }
        private void Conectar()
        {
            if (mqtt.ConnectionState == ConnectionState.Disconnected)
            {
                mqtt.Connect("IoTMovil");
                UltimoComando = "Conectando...";

                Notify("UltimoComando");
            }
        }

        public void ActualizarLecturas()
        {
            lecturasTemperatura = lecturaManager.LecturasModelsDeSensorEnRango("5c8a7b7b7a299c32349f4388", DateTime.Now.AddHours(-5), DateTime.Now, " °C").ToList();
            lecturasHumedad = lecturaManager.LecturasModelsDeSensorEnRango("5c8a7b897a299c32349f4389", DateTime.Now.AddHours(-5), DateTime.Now, " %").ToList();
            lecturasLuminosidad = lecturaManager.LecturasModelsDeSensorEnRango("5c8a4a979629482d5c462d94", DateTime.Now.AddHours(-5), DateTime.Now, " L").ToList();

            //Graficar(lecturasTemperatura, GraficoTemperatura, "Temperatura");
            //Notify("GraficoTemperatura");
            //Graficar(lecturasHumedad, GraficoHumedad, "Humedad");
            //Notify("GraficoHumedad");
            //Graficar(lecturasLuminosidad, GraficoLuminosidad, "Luminosidad");
            //Notify("GraficoLuminosidad");
        }

        private void Graficar(List<ModelLectura> datos, PlotModel grafico, string titulo)
        {
            grafico = new PlotModel();
            LinearAxis axisX = new LinearAxis();
            axisX.Position = AxisPosition.Bottom;
            LinearAxis axisY = new LinearAxis();
            axisY.Position = AxisPosition.Left;
            grafico.Axes.Add(axisX);
            grafico.Axes.Add(axisY);
            grafico.Title = titulo;
            LineSeries linea = new LineSeries();
            int i = 0;
            foreach (var punto in datos)
            {
                linea.Points.Add(new DataPoint(i, punto.Lectura.Valor));
                i++;
            }
            linea.Color = OxyColors.DarkBlue;
            grafico.Series.Add(linea);
            
        }

    }
}
