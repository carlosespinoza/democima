﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DemoIoTCIMA
{
    public abstract class ViewModelBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected readonly IMessageService messageService;
        public ViewModelBase()
        {
            messageService = new MessageService();
        }
        protected void Notify(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
