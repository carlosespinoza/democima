﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoIoTCIMA
{
    public  class MessageService : IMessageService
    {
        public async Task MostrarAlerta(string mensaje)
        {
            await App.Current.MainPage.DisplayAlert("CursoIoT", mensaje, "Ok");
        }

        public async Task<string> MostrarAlertaConOpciones(string mensaje, List<string> opciones)
        {
            return await App.Current.MainPage.DisplayAlert("CursoIoT", mensaje, opciones[0], opciones[1]) ? opciones[0] : opciones[1];
        }

        public async Task<string> MostrarOpciones(string mensaje, List<string> opciones)
        {
            return await App.Current.MainPage.DisplayActionSheet(mensaje, "Cancelar", null, opciones.ToArray());
        }

    }
}